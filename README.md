# **All Programming Is Bookkeeping**

Programmers tend to loathe writing bookkeeping software. Just thinking about doing something so mundane and un-sexy as writing a double-entry bookkeeping system is probably enough to make a whole raft of programmers contemplate switching careers in earnest.

If a program works well that means that all the results come out exactly as expected, there is no room at all for even a little bit of uncertainty in the result. So in case you will compose amend programming the internals of that product will look especially like an accounting program!

**Let me give you a concrete example of what I mean to clarify this:**

Imagine you’re building a computer system to administer a telephone switch. The switch has an entire pack of lines connected to it, a few lines are 'trunks', lines with which you can forward messages to other phone switches, and a few lines that connect to subscribers. Each subscriber is represented by a a wire pair and by manipulating the signals on the wire and by listening to the wires you can communicate with the phone on the other side.

Our alternatives for the wires to the endorsers are: recognize the telephone going 'off snare', identify the telephone going 'on snare', distinguishing a short over the wires that endures <50 ms and so on. The trunk lines are much the same as the subscriber lines, only difference is that you have a limited number of them to route calls ‘out’, and instead being labeled with supporter ids they are labeled with their long-separate prefixes.

A bunch of hardware will take care of abstracting all this and presents a nice and clean port-mapped interface for your phone switchboard, or maybe it appears as a chunk of memory. If you’re doing all this in the present time then you’re going to be talking some digital protocol to a primary rate interface or some chunk of hardware further upstream using TCP/IP.

No matter how it is implemented though, you’re going to be looking after incoming calls, outgoing calls, subscribers that are available to be called, subscribers that are setting up calls, subscribers with calls in progress and trunks that are engaged, free or used for dialing or in-system messaging.

At whatever point a call finishes begins or closures you'll need to log that call for charging purposes et cetera

If you don’t approach this like you would approach a [bookkeeping.com]( https://bookkeeping.com/) problem with checks and balances you’re going to be tearing out your hair (or it’ll be prematurely grey) about inquiries, for example, "For what reason do we have such a large number of dropped calls?" and "Why the charging doesn't coordinate the endorsers memory?

**To fix this here is what you can do:**

For every interaction in the system you assign a simple counter. Phone goes off-hook: counter. Phone goes on-hook: counter. Call-start: counter. Call-end: counter. Brings in-advance: number (that is a harder one, however it ought to be increased at whatever point a call is set-up and decremented when a call closes), and so on, and so forth. And then you go and define all the ways in which these numbers relate to each other. At any given point in time the quantity of rings set - the quantity of calls separated ought to be equivalent to the quantity of brings in advance. The number of calls in progress should always be >= 0. The number of ticks billed to the user should equal the number of ticks the subscriber lines were ‘in call’ on non-local numbers and that number in turn be equivalent to the quantity of ticks on the trunks.

**You can utilize this data in a few ways**

When you’re debugging: When you are searching for a bug the information in the counters can guide you to the location of a potential problem. The counters and variables have all these nicely defined relations and if one of the relations does not work out then it will stand out like a big red flashing light. This indicates either that your understanding of how the system works is not accurate, that the system does not work in the way it should or that there is an alternative path through the code that wasn’t properly instrumented. Either way, something needs to be repaired to get the bookkeeping to match again.

* **when you're trying:** you characterize various situations and how you figure they should influence the different counters and factors.Then you set up your tests, run them and check the state of the counters and variables after every test has run to make sure you get the desired result In case you're doing longer trials you should need to have a huge number of simultaneous sessions to put the framework through hell and afterward you check a short time later if the total of the different counters and factors precisely reflects what you did.

* To give you certainty that you have to be sure completely aced the framework and that there are no indistinct states or lumps of un-instrumented code that have impacts on yourbilling or internal [bookkeeping.com]( https://bookkeeping.com/). This will help with you getting enough sleep at night.

* that bookeeping is useful to the point that I more often than not keep it around in diligent capacity (regularly, a database table) on an every moment or per-hour premise. That way I can reach back in time if there is anything amiss and I can get within an hour of the introduction of a problem. That’s usually close enough to be able to narrow down where in the system the problem is located.

In many dialects you would then be able to attest that these conditions are valid whenever amid execution of your tests (for the most part with something many refer to as 'assert').These are called ’invariants’ (things that should always hold true while your program runs).

At last all writing computer programs is accounting, notwithstanding when at first look it has nothing to do with accounting.
